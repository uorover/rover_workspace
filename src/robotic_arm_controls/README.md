# robotic_arm_controls Package

This package is meant to house code related to the development of the robotic arm.

The code in this package was initially migrated from our old `robotic_arm_package` package that was meant for ROS Melodic.

## TODO

- test the code to ensure it works as expected (the migration to ROS2 might have left bugs)
- update this README with a description of the nodes, topics, launch files, etc.
