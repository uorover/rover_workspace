/*
 *  
 *  uOttawa Mars Rover Team
 *  
 *  Robotic Arm V2.1 Controls
 *  
 *  Adapted to new AMT222 Absolute Encoders
 *  
 *  
 *  
 *  
 *  Contributors:
 *  - Justin Zhang      jzhan558@uottawa.ca
 *  - Arnav Chaturvedi  achat055@uottawa.ca
 *  - Sameed Ahmed      sahme286@uottawa.ca
 *  - Olivier Caron     ocaro009@uottawa.ca
 *  
 *  
 *  Up-to-date as of 2024-02-26
 *  
 *  
 */
